import asyncio

async def hola():
    print("¡Hola, mundo!")

def main():
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(hola())
    finally:
        loop.close()

if __name__ == '__main__':
    main()