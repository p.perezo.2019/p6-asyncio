import socket
import json

# Datos de la oferta SDP
offer_sdp = {
    'version': 0,
    'origin': {
        'username': 'user',
        'sessionId': 434344,
        'sessionVersion': 0,
        'netType': 'IN',
        'ipVer': 4,
        'address': '127.0.0.1'
    },
    'name': 'Session',
    'timing': {
        'start': 0,
        'stop': 0
    },
    'connection': {
        'version': 4,
        'ip': '127.0.0.1'
    },
    'media': [
        {
            'rtp': [
                {'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                {'payload': 96, 'codec': 'opus', 'rate': 48000}
            ],
            'type': 'audio',
            'port': 54400,
            'protocol': 'RTP/SAVPF',
            'payloads': '0 96',
            'ptime': 20,
            'direction': 'sendrecv'
        },
        {
            'rtp': [
                {'payload': 97, 'codec': 'H264', 'rate': 90000},
                {'payload': 98, 'codec': 'VP8', 'rate': 90000}
            ],
            'type': 'video',
            'port': 55400,
            'protocol': 'RTP/SAVPF',
            'payloads': '97 98',
            'direction': 'sendrecv'
        }
    ]
}

# Convertir la oferta SDP a JSON
offer_json = json.dumps(offer_sdp)

# Configuración del socket UDP
udp_host = '127.0.0.1'
udp_port = 12345

# Crear un socket UDP
udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Enviar la oferta SDP a través del socket UDP
udp_socket.sendto(offer_json.encode(), (udp_host, udp_port))

print("Oferta SDP enviada a", udp_host, "puerto", udp_port)

# Cerrar el socket
udp_socket.close()